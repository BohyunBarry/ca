CC=gcc
CFLAGS= -g -Wall -O3
TARGET = simulate
SRC = $(wildcard *.c)
OBJ = $(patsubst %.c, %.o, $(SRC))

$(TARGET) : $(OBJ) $(wildcard *.h)
	$(CC) $(CFLAGS) -o $(TARGET) $^

%.o : %.c
	$(CC) $(CFLAGS) -c $< -o $@

demo : demo1 demo2 demo3

demo1 : $(TARGET)
	ca1_jobs.pl 10 10 10 | ./$(TARGET) | tee demo_output1.txt

demo2 : $(TARGET)
	ca1_jobs.pl 20 20 20 | ./$(TARGET) | tee demo_output2.txt

demo3 : $(TARGET)
	ca1_jobs.pl 30 30 30 | ./$(TARGET) | tee demo_output3.txt

.PHONY: clean
clean :
	rm -f $(TARGET)
	rm -f $(wildcard *.o)
	rm -f demo_output.txt
	rm -f demo_output1.txt
	rm -f demo_output2.txt
	rm -f demo_output3.txt
