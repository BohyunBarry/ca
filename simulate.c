#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include "data.h"
#define JN 50

struct Job jobs[JN];
struct Job fifoJ[JN];
struct Job sjfJ[JN];
struct Job stcfJ[JN];
struct Job rr1J[JN];
struct Job rr2J[JN];
struct Job newJob(struct Job j);

struct Parameter fifoP[JN];
struct Parameter sjfP[JN];
struct Parameter stcfP[JN];
struct Parameter rr1P[JN];
struct Parameter rr2P[JN];
struct Parameter newPar(struct Parameter p);

void getArrivalTime(struct Job j[], int n, int v, struct Job re[]);
void makeToList(int position, struct Job j[]);
void setEnd(struct Parameter p[], char *pid, int n, int end);
void setStart(struct Parameter p[], char *pid, int n, int start);
void movePosition(struct Job j[], int count);

int calculateTurnaroundTime(struct Parameter p);
int calculateResponseTime(struct Parameter p);
double calculateAvgTurnaroundTime(struct Parameter p[], int n);
double calculateAvgResponseTime(struct Parameter p[], int n);

int runtime_com(const void *j1, const void *j2);//compare two jobs running time
int arrvaltime_com(const void *j1, const void *j2);//compare two jobs arrival time
int stcf_com(const void *j1, const void *j2);//compare two jobs shortest time to completion first

int runtime_com(const void *j1, const void *j2){
  int f1 = ((struct Job *)j1)->run;
  int f2 = ((struct Job *)j2)->run;
  return (f1 - f2);
}
int arrivaltime_com(const void *j1, const void *j2){
  int f1 = ((struct Job *)j1)->arrival;
  int f2 = ((struct Job *)j2)->arrival;
  return (f1 -f2);
}


short jobCount = 0;
short slice1 = 1;
short slice2 = 2;

int main(void){
  int totalRuntime = 0;
  char *line = NULL;
  size_t size;
  size_t read;
  while((read = getline(&line, &size, stdin) > 0) && jobCount < JN){
	printf("? %s", line);
	char *token = strtok(line, " ");
    char *jobId = token;
    int arrival = atoi(strtok(NULL, " "));
    int run = atoi(strtok(NULL, " "));
    totalRuntime += run;
    jobs[jobCount].arrival = arrival;
    jobs[jobCount].run = run;
    strcpy(jobs[jobCount].pid, jobId);

    struct Parameter p;
    strcpy(p.pid, jobId);
    p.arrival = arrival;
    p.start = -1;
    p.end = 0;
    fifoP[jobCount].arrival = arrival;
    fifoP[jobCount].start = -1;
    fifoP[jobCount].end = 0;
    strcpy(fifoP[jobCount].pid, jobId);

    sjfP[jobCount].arrival = arrival;
    sjfP[jobCount].start = -1;
    sjfP[jobCount].end = 0;
    strcpy(sjfP[jobCount].pid, jobId);

    stcfP[jobCount].arrival = arrival;
    stcfP[jobCount].start = -1;
    stcfP[jobCount].end = 0;
    strcpy(stcfP[jobCount].pid, jobId);

    rr1P[jobCount].arrival = arrival;
    rr1P[jobCount].start = -1;
    rr1P[jobCount].end = 0;
    strcpy(rr1P[jobCount].pid, jobId);

    rr2P[jobCount].arrival = arrival;
    rr2P[jobCount].start = -1;
    rr2P[jobCount].end = 0;
    strcpy(rr2P[jobCount].pid, jobId);

    jobCount++;
  }
  
  //make a new array
    memcpy(&fifoJ, &jobs, sizeof(jobs));
    memcpy(&stcfJ, &jobs, sizeof(jobs));
    memcpy(&sjfJ, &jobs, sizeof(jobs));
    memcpy(&rr1J, &jobs, sizeof(jobs));
    memcpy(&rr2J, &jobs, sizeof(jobs));

  printf("\nT\tFIFO\tSJF\tSTCF\tRR1\tRR2\n");
  
  int timeUnit = 0, fifoCount = 0, stcfCount = 0, sjfCount = 0, rr1Count = 0, rr2Count = 0;
  int rr1CPUCount = 0, rr2CPUCount = 0;
  while(1){
    struct Job jobArrived[jobCount];
    memset(jobArrived, 0, sizeof(jobArrived));
    getArrivalTime(jobs, jobCount, timeUnit, jobArrived);
    for(int i = 0; i < jobCount; i++){
      if(jobArrived[i].run){
	fifoJ[fifoCount] = newJob(jobArrived[i]);
	stcfJ[stcfCount] = newJob(jobArrived[i]);
	sjfJ[sjfCount] = newJob(jobArrived[i]);
	rr1J[rr1Count] = newJob(jobArrived[i]);
	rr2J[rr2Count] = newJob(jobArrived[i]);

    rr2Count++;
    rr1Count++;
    fifoCount++;
    stcfCount++;
    sjfCount++;
      }
    }
	memset(jobArrived, 0, sizeof(jobArrived));

    /*fifo*/
    if(fifoJ[0].arrival <= timeUnit && fifoJ[0].run > 0 && fifoCount > 0){
      setStart(fifoP, fifoJ[0].pid, jobCount, timeUnit);
      fifoJ[0].run--;
    }else{
      printf("\t");
    }

    /*sjf*/
    if(sjfJ[0].arrival <= timeUnit && sjfJ[0].run > 0 && sjfCount > 0){
      setStart(sjfP, sjfJ[0].pid, jobCount, timeUnit);
      sjfJ[0].run--;
    }else{
      printf("\t");
    }

    /*stcf*/
    qsort((void *)stcfJ, stcfCount, sizeof(stcfJ[0]), runtime_com);
    if(stcfJ[0].arrival <= timeUnit && stcfJ[0].run > 0 && stcfCount > 0){
      setStart(stcfP, stcfJ[0].pid, jobCount, timeUnit);
      stcfJ[0].run--;
    }else{
      printf("\t");
    }

    /*rr1*/
    if(rr1J[0].run > 0 && rr1J[0].arrival <= timeUnit && rr1Count > 0){
      if(rr1CPUCount == slice1){
	rr1CPUCount = 0;
	movePosition(rr1J, rr1Count);
      }
      setStart(rr1P, rr1J[0].pid, jobCount, timeUnit);
      rr1J[0].run--;
      rr1CPUCount++;
    }else{
      printf("\t");
    }

    /*rr2*/
    if(rr2J[0].run > 0 && rr2J[0].arrival <= timeUnit && rr2Count > 0){
      if(rr2CPUCount == slice2){
	rr2CPUCount = 0;
	movePosition(rr2J, rr2Count);
      }
      setStart(rr2P, rr2J[0].pid, jobCount, timeUnit);
      rr2J[0].run--;
      rr2CPUCount++;
    }else{
      printf("\t");
    }

    //Ending time
    if(fifoJ[0].run == 0 && fifoCount > 0){
      setEnd(fifoP, fifoJ[0].pid, jobCount, timeUnit);
      makeToList(0, fifoJ);
      fifoCount--;
    }
    if(sjfJ[0].run == 0 && sjfCount > 0){
      setEnd(fifoP, fifoJ[0].pid, jobCount, timeUnit);
      makeToList(0, sjfJ);
      sjfCount--;
      qsort((void *)sjfJ, sjfCount, sizeof(sjfJ[0]),runtime_com);
    }
    if(stcfJ[0].run == 0 && stcfCount > 0){
      setEnd(stcfP, stcfJ[0].pid, jobCount, timeUnit);
      makeToList(0, stcfJ);
      stcfCount--;
    }
    if(rr1J[0].run == 0 && rr1Count > 0){
      setEnd(rr1P, rr1J[0].pid, jobCount, timeUnit);
      makeToList(0, rr1J);
      rr1Count--;
      rr1CPUCount = 0;
    }
    if(rr2J[0].run == 0 && rr2Count > 0){
      setEnd(rr2P, rr2J[0].pid, jobCount, timeUnit);
      makeToList(0, rr2J);
      rr2Count--;
      rr2CPUCount = 0;
    }
    printf("\n");
    timeUnit++;
    if(timeUnit > totalRuntime + 10)
      break;
  }
  printf("= SIMULATION COMPLETE\n");
  printf("#\tNAME\tFIFO\tSJF\tSTCF\tRR1\tRR2\n");
  for(int i = 0; i < jobCount; i++){
    printf("T\t%s\t%d\t%d\t%d\t%d\t%d\n", stcfP[i].pid,
	   calculateTurnaroundTime(fifoP[i]), calculateTurnaroundTime(sjfP[i]),
	   calculateTurnaroundTime(stcfP[i]), calculateTurnaroundTime(rr1P[i]),
	   calculateTurnaroundTime(rr2P[i]));
  }
  for (int i = 0; i < jobCount; i++)
    {
      printf("R\t%s\t%d\t%d\t%d\t%d\t%d\n", stcfP[i].pid,
	     calculateResponseTime(fifoP[i]), calculateResponseTime(sjfP[i]),
	     calculateResponseTime(stcfP[i]), calculateResponseTime(rr1P[i]),
	     calculateResponseTime(rr2P[i]));
    }
  printf("= INDIVIDUAL COMPLETE\n");
  printf("#\tSCHEDULER\tAVG_TURNAROUND\tAVG_RESPONSE\n");
  {
    printf("@\tFIFO\t\t%f\t\t%f\n", calculateAvgTurnaroundTime(fifoP, jobCount), calculateAvgResponseTime(fifoP, jobCount));
    printf("@\tSJF\t\t%f\t\t%f\n", calculateAvgTurnaroundTime(sjfP, jobCount), calculateAvgResponseTime(sjfP, jobCount));
    printf("@\tSTCF\t\t%f\t\t%f\n", calculateAvgTurnaroundTime(stcfP, jobCount), calculateAvgResponseTime(stcfP, jobCount));
    printf("@\tRR1\t\t%f\t\t%f\n", calculateAvgTurnaroundTime(rr1P, jobCount), calculateAvgResponseTime(rr1P, jobCount));
    printf("@\tRR2\t\t%f\t\t%f\n", calculateAvgTurnaroundTime(rr2P, jobCount), calculateAvgResponseTime(rr2P, jobCount));
  }
  printf("= AGGREGATE COMPLETE\n");    printf("#\tSCHEDULER\tAVG_TURNAROUND\tAVG_RESPONSE\n");
  {
    printf("@\tFIFO\t\t%f\t\t%f\n", calculateAvgTurnaroundTime(fifoP, jobCount), calculateAvgResponseTime(fifoP, jobCount));
    printf("@\tSJF\t\t%f\t\t%f\n", calculateAvgTurnaroundTime(sjfP, jobCount), calculateAvgResponseTime(sjfP, jobCount));
    printf("@\tSTCF\t\t%f\t\t%f\n", calculateAvgTurnaroundTime(stcfP, jobCount), calculateAvgResponseTime(stcfP, jobCount));
    printf("@\tRR1\t\t%f\t\t%f\n", calculateAvgTurnaroundTime(rr1P, jobCount), calculateAvgResponseTime(rr1P, jobCount));
    printf("@\tRR2\t\t%f\t\t%f\n", calculateAvgTurnaroundTime(rr2P, jobCount), calculateAvgResponseTime(rr2P, jobCount));
  }
  printf("= AGGREGATE COMPLETE\n");

  return 0;
}
void getArrivalTime(struct Job j[], int n, int v, struct Job re[]){
  int temp = 0;
  for(int i = 0; i < n; i++){
    if(j[i].arrival == v){
      re[temp] = j[i];
      temp++;
    }
  }
}

struct Job newJob(struct Job j){
  struct Job j1;
  j1.arrival = j.arrival;
  j1.run = j.run;
  strcpy(j1.pid, j.pid);
  return j1;
}

void makeToList(int position, struct Job j[]){
  for(int i = position - 1; i < jobCount; i++){
    j[i] = j[i + 1];
  }
}

void movePosition(struct Job j[], int count){
  j[count] = j[0];
  makeToList(0, j);
}

struct Parameter newPar(struct Parameter p){
  struct Parameter p1;
  p1.start = p.start;
  p1.end = p.end;
  strcpy(p1.pid, p.pid);
  return p1;
}

int calculateTurnaroundTime(struct Parameter p){
  return p.end - p.start;
}

int calculateResponseTime(struct Parameter p){
  return p.start - p.end;
}

void setEnd(struct Parameter p[], char *pid, int n, int end){
  for(int i = 0; i < n; i++){
    if(strcmp(p[i].pid, pid) == 0){
      p[i].end = end;
      return;
    }
  }
}

void setStart(struct Parameter p[], char *pid, int n, int start){
  for(int i = 0; i < n; i++){
    if(strcmp(p[i].pid, pid) == 0){
      if(p[i].start == -1){
	p[i].start = start;
      }
      return;
    }
  }
}

double calculateAvgTurnaroundTime(struct Parameter p[], int n){
  int total = 0;
  for(int i = 0; i < n; i++){
    total += calculateTurnaroundTime(p[i]);
  }
  return total / n;
}

double calculateAvgResponseTime(struct Parameter p[], int n){
  int total = 0;
  for(int i = 0; i < n; i++){
    total += calculateResponseTime(p[i]);
  }
  return total / n;
}


