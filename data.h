#ifndef _DATA_H_
#define _DATA_H_

typedef enum {FIFO, SJF, SCTF, RR1, RR2} scheduler;

struct Parameter{
  char pid[10];
  int start;
  int end;
  int arrival;
};

struct Job{
  char pid[10];
  int arrival;
  int run;
};

#endif
